'use client';
import React, { useEffect, useState } from "react";
import NavbarDesktop from "./navbarDesktop";
import NavbarMobile from "./navbarMobile";

export default function Navbar() {

    const [desktopView, setDesktopView] = useState(isDesktopView());

    function isDesktopView() : boolean {
        if (typeof window !== "undefined") {
            return window.innerWidth > 750;
        }
        return true;
    }
    
    const updateViewType = () => {
        setDesktopView(isDesktopView());
    };

    useEffect(() => {
        if (typeof window !== "undefined") {
            window.addEventListener("resize", updateViewType);
            return () => {
                window.removeEventListener("resize", updateViewType);
            }
        }
    },[updateViewType]);

    return (
        <div>
            {desktopView ? <NavbarDesktop/> : <NavbarMobile/> }
        </div>
    );
}