import React, { ReactNode } from 'react';
import Navbar from '../component/navigation/navbar'

type Props = {
  children: ReactNode;
  title?: string;
};

export default function Layout({ children }: Props) {
    return (
      <html lang="fr">
        <head>
          <title>Stromes</title>
        </head>
        <body>
            <Navbar></Navbar>
            <div>
                {children}
            </div>
        </body>
      </html>
      )
  }